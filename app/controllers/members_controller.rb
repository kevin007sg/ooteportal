class MembersController < ApplicationController
  include VenuesHelper
  # uncomment to ensure common layout for forms
  # layout  "sign", :only => 
  
  before_action :authorized_admin_team_leader, except: [:change_tenant]

  def new()
    @member = Member.new()
    @user   = User.new()
  end

  def create()
    @user = User.new( user_params )
    params[:member][:active_status] = true

    # ok to create user, member
    if !User.where(email: @user.email).any? 
      if @user.save_and_invite_member() && Member.create_member( @user, member_params )
        flash[:notice] = "New member added and invitation email sent to #{@user.email}."
        redirect_to root_path
      else
        #flash[:error] = "errors occurred!"
        @member = Member.new( member_params ) # only used if need to revisit form
        MemberLogger.add_log(@member.id, "Member created by #{current_user.member.first.full_name}")
        render :new
      end
    else
      @exist_user = User.where(email: @user.email).first
      if @exist_user.tenants.where(id: Tenant.current_tenant.id).size == 0
        @exist_user.tenants << Tenant.current_tenant
        Member.create_member(@exist_user, member_params)
        @member = @exist_user.member
        MemberLogger.add_log(@member.ids.first, "Member created by #{current_user.member.first.full_name}")
        flash[:notice] = "#{@user.email} is added to #{Tenant.current_tenant.name}"
        MemberMailer.with(email: @user.email, member: @member).new_invite_memmber_email.deliver_now

      else
        flash[:danger] = "#{@user.email} is already added to #{Tenant.current_tenant.name}"
      end
      redirect_to root_path
    end

  end
  
  def edit
    @member = Member.find(params[:id])
    @member_logs = MemberLogger.where(member_id: @member.id).order(created_at: :desc)
  end
  
  def update
    @member = Member.find(params[:id])
    old_member = @member.to_member_string
    coordinates = get_coordinates(params[:member][:street_name])
    @member_logs = MemberLogger.where(member_id: @member.id).order(created_at: :desc)
    
    if coordinates.is_a?(String)
      @member.errors.add(:street_name, coordinates)
      render :edit
    else
      params[:member][:latitude] = coordinates[0]
      params[:member][:longitude] = coordinates[1]
      
      respond_to do |format|
        
        if @member.update(member_params)
          MemberLogger.add_log(@member.id, "Member updated by #{current_user.member.first.full_name} - [ Previous Details -> #{old_member} ] - [ New Details -> #{@member.to_member_string} ]")
          format.html { redirect_to members_path, notice: 'Member updated successfully.' }
        else
          
          format.html { render :edit }
          format.json { render json: @member.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  def index
    #@members = Member.all.paginate(page: params[:page], per_page: 10).order(:last_name)
  end
  
  def display_events_by_member
    @member_name = Member.find(params[:format]).full_name
    @member_events = Member.find(params[:format]).events.order(start_date_time: :desc).paginate(page: params[:page], per_page: 10)
  end
  
  def reactivate_member
    @member = Member.find(params[:format])
    @member.active_status = true
    @member.save
    flash[:notice] = "#{@member.full_name} is successfully reactivated."
    redirect_to members_path
  end
  
  def destroy
    @member = Member.find(params[:id])
    #@member.destroy
    @member.active_status = false
    @member.save
    
    flash[:notice] = "#{@member.full_name} is successfully suspended."
    redirect_to members_path
  end
  
  def change_tenant
    @tenant = Tenant.find_by_hashid(params["id"])
    session[:tenant_id] = @tenant.id
    Tenant.set_current_tenant(@tenant.id)
    redirect_to root_path
  end
  
  def search_members
    @members = Member.search_members(params)
    if @members
      respond_to do |format|
        format.js {render partial: 'members/display_members', obj: @members }
      end
    end
  end
  
  def display_members_report

  end

  def search_members_report
    @members = Member.search_members_report(params)
    @from_start_date = (params[:from_start_date].present? ? params[:from_start_date].to_time : 6.months.ago).strftime(Event.date_only_format)
    @end_start_date = (params[:to_start_date].present? ? params[:to_start_date].to_time : 1.days.from_now).strftime(Event.date_only_format)
    
    if @members
      respond_to do |format|
        format.js { render partial: 'members/display_members_report', obj: @members, from_start_date: @from_start_date, end_start_date: @end_start_date }
        format.xlsx         {
            response.headers[
              'Content-Disposition'
            ] = "attachment; filename=member-report.xlsx"
          }
      end
    end
  end

  private

  def member_params()
    params.require(:member).permit(:first_name, :last_name, :mobile_number, :region, :description, :street_name, :latitude, :longitude, :active_status, :role, team_ids:[] )
  end

  def user_params()
    params.require(:user).permit(:email, :password, :password_confirmation, :role)
  end

end
