class RegistrationsController < Milia::RegistrationsController
  include VenuesHelper
  
  before_action :authenticate_user!
  before_action :redirect_unless_admin, only: [:new, :create]
  skip_before_action :require_no_authentication
  
  def new
    super 
  end

  def create
      # have a working copy of the params in case Tenant callbacks
      # make any changes
    tenant_params = sign_up_params_tenant
    user_params   = sign_up_params_user
    current_tenant = Tenant.current_tenant
  
    #sign_out_session!
       # next two lines prep signup view parameters
    prep_signup_view( tenant_params, user_params, nil )
  
     # validate recaptcha first unless not enabled
    Tenant.transaction  do
      @tenant = Tenant.create_new_tenant( tenant_params, user_params, nil)
      if @tenant.errors.empty?   # tenant created


        user_existed = User.where(email: user_params["email"]).exists? 
        initiate_tenant( @tenant )    # first time stuff for new tenant
        
        if user_existed
          resource = User.where(email: user_params["email"]).first
          @tenant.users << resource
        else
          resource = User.create(user_params)
        end
        
        if resource.errors.empty?   #  SUCCESS!

          log_action( "signup user/tenant success", resource )
            # do any needed tenant initial setup
          
          member = Tenant.tenant_signup(resource, @tenant)
          
          if user_existed
            MemberMailer.with(email: user_params["email"], member: member).new_invite_tenant_admin_email.deliver_now
          end
          
          session[:tenant_id] = current_tenant.id
          redirect_to root_path, notice: "#{@tenant.name} Portal created successfully. An invitation email is sent to #{resource.email}"

          
        else  # user creation failed; force tenant rollback
          flash[:error] = "Password do not match."
          log_action( "signup user create failed", resource )
          raise ActiveRecord::Rollback   # force the tenant transaction to be rolled back
        end  # if..then..else for valid user creation

      else
        #resource.valid?
        flash[:error] = "Tenant existed."
        log_action( "tenant create failed", @tenant )
        render :new
      end # if .. then .. else no tenant errors

    end  #  wrap tenant/user creation in a transaction

  

  
  end 


  def update
    
    @member = Member.find(current_user.member.first.id)
    
    
    coordinates = get_coordinates(params[:member][:street_name])
    
    if coordinates.is_a?(String)
      @member.errors.add(:street_name, coordinates)
      render :edit
    else
    
      params[:member][:latitude] = coordinates[0]
      params[:member][:longtitude] = coordinates[1]
      
      if current_user.member.first.try(:admin?)
        @member.skip_team_validation = true
      end
      
      
      if @member.update(member_params)
        super
      else
        #flash[:notice] = "Error occured updating member."
        render :edit
      end
    end
  end

  def edit
    @member = current_user.member.first
  end
  
  private
  
  def redirect_unless_admin
    Tenant.set_current_tenant session[:tenant_id]
    unless Tenant.current_tenant.name == "Landlord"
      flash[:error] = "Only landlords can create new tenants."
      redirect_to home_index_path
    end
  end
  
  def member_params
    params.require(:member).permit(:first_name, :last_name, :mobile_number, :region, :description, :street_name, :latitude, :longitude, :pdpa, team_ids:[])
  end
  
  def after_sign_up_path_for(resource)
    new_member_registration_path
  end
  
end