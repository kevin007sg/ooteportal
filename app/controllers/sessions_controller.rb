class SessionsController < Milia::SessionsController
  
  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message!(:notice, :signed_in)
    sign_in(resource_name, resource)
    yield resource if block_given?
    session[:tenant_id] = current_user.default_tenant_id
    respond_with resource, location: after_sign_in_path_for(resource)
  end
  
  def destroy
    session[:tenant_id] = nil
    super
  end
  
end