class SettingsController < ApplicationController
  before_action :set_setting, only: [:edit, :update]
  before_action :authorized_admin

  # GET /settings
  # GET /settings.json
  def index
    if Setting.first.blank?
      Setting.create(send_sms: false)
    end
    @setting = Setting.first
    @setting_logs = SettingLogger.where(setting_id: @setting.id).order(created_at: :desc)
  end

  # GET /settings/1
  # GET /settings/1.json
  def show
  end

  # GET /settings/new
  def new
    @setting = Setting.new
  end

  # GET /settings/1/edit
  def edit
  end

  # POST /settings
  # POST /settings.json
  def create
    @setting = Setting.new(setting_params)

    respond_to do |format|
      if @setting.save
        format.html { redirect_to @setting, notice: 'Setting was successfully created.' }
        format.json { render :show, status: :created, location: @setting }
      else
        format.html { render :new }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /settings/1
  # PATCH/PUT /settings/1.json
  def update
    old_setting = @setting.to_setting_string
    respond_to do |format|
      if @setting.update(setting_params)
        SettingLogger.add_log(@setting.id, "Setting updated by #{current_user.member.first.full_name} - [ Previous Details -> #{old_setting} ] - [ New Details -> #{@setting.to_setting_string} ]")
        format.html { redirect_to settings_path, notice: 'Setting was successfully updated.' }
        format.json { render :show, status: :ok, location: @setting }
      else
        format.html { render :edit }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /settings/1
  # DELETE /settings/1.json
  def destroy
    @setting.destroy
    respond_to do |format|
      format.html { redirect_to settings_url, notice: 'Setting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_setting
      @setting = Setting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def setting_params
      params.require(:setting).permit(:tenant_id, :send_sms, :manage_event_report, :manage_attendance, :display_phone, :manage_event_code, :display_rhq)
    end
end
