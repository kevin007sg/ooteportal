class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_only, except: [:show]

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    unless current_user.admin?
      unless @user == current_user
        redirect_to root_path, :alert => "Access denied."
      end
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(secure_params)
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    redirect_to users_path, :notice => "User deleted."
  end
  
  def display_users_by_portal
    
    @tenant = Tenant.find_by_hashid(params[:format])
    @users = User.find_by_sql ['SELECT "users"."id", first_name, last_name, "users"."created_at", "users"."email" FROM "users" INNER JOIN "members" ON "members"."user_id" = "users"."id" AND (members.tenant_id = ?) WHERE (users.tenant_id IS NULL) ORDER BY first_name', @tenant.id]
    
  end
  
  def edit_email
    @user = User.find_by_hashid(params[:format])
  end
  
  def update_email
    @user = User.find_by_hashid(params[:id])
    
    if @user.update(email: params[:user][:email])
      redirect_to root_path, :notice => "Email updated. Please advise member to confirm their new email in the mailbox. Until email is confirmed by member, the email below will not be reflect the new email."
    else
      redirect_to edit_email_path(params[:id]), :alert => "Unable to update email. Please try again."
    end
  end

  private

  def admin_only
    unless current_user.member.first.try(:admin?)
      redirect_to root_path, :alert => "Access denied."
    end
  end

  def secure_params
    params.require(:user).permit(:role, :default_tenant_id, :email)
  end

end
