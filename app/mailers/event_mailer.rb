class EventMailer < ApplicationMailer
  
  def new_event_email
    Tenant.set_current_tenant(params[:tenant])
    @event = Event.find(params[:event][:id])
    @m = params[:member]
    make_bootstrap_mail(to: @m.user.email, subject: "[ #{Tenant.current_tenant.name} ] New Event Assigned - #{@event.name}")
  end
  
  def update_event_email
    Tenant.set_current_tenant(params[:tenant])
    @event = Event.find(params[:event][:id])
    @m = params[:member]
    make_bootstrap_mail(to: @m.user.email, subject: "[ #{Tenant.current_tenant.name} ] Updated Event - #{@event.name}")
  end
  
  def cancel_event_email
    Tenant.set_current_tenant(params[:tenant])
    @event = Event.find(params[:event][:id])
    @m = params[:member]
    make_bootstrap_mail(to: @m.user.email, subject: "[ #{Tenant.current_tenant.name} ] Cancelled Event - #{@event.name}")
  end
  
  def event_reminder_email
    Tenant.set_current_tenant(params[:tenant])
    @event = Event.find(params[:event][:id])
    @m = params[:member]
    make_bootstrap_mail(to: @m.user.email, subject: "[ #{Tenant.current_tenant.name} ] Reminder of Your Upcoming Event - #{@event.name}")
  end
  
  def new_event_creation_email
    Tenant.set_current_tenant(params[:tenant])
    @event = Event.find(params[:event][:id])
    @m = params[:member]
    make_bootstrap_mail(to: @m.user.email, subject: "[ #{Tenant.current_tenant.name} ] New Event Created - #{@event.name}")
  end
  
  def event_unassigned_reminder_email
    
    Tenant.set_current_tenant(params[:tenant])
    @event = Event.find(params[:event][:id])
    @m = params[:member]
    make_bootstrap_mail(to: @m.user.email, subject: "[ #{Tenant.current_tenant.name} ] #{@event.status} Event - #{@event.name}")
  end
  
  def unassigned_members_from_event_email
    Tenant.set_current_tenant(params[:tenant])
    @event = Event.find(params[:event][:id])
    @m = Member.find(params[:member]["id"])
    make_bootstrap_mail(to: @m.user.email, subject: "[ #{Tenant.current_tenant.name} ] Event Unassigned - #{@event.name}")
  end
end
