class SettingLogger < ApplicationRecord
  acts_as_tenant
  
  has_one :setting
  
  include Hashid::Rails
  
  def self.add_log(setting_id, log_text)
    SettingLogger.create(setting_id: setting_id, setting_log: "[ #{Time.current.strftime("%d-%b-%Y - %l:%M %p")} ] : #{log_text}.")
  end  
end
