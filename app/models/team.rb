class Team < ApplicationRecord
  
  
  acts_as_tenant
  
  has_many :team_members
  has_many :members, through: :team_members


  validates :name, presence: true
  validates_uniqueness_of :name, :scope => :tenant_id


  include Hashid::Rails
  
  def team_descriptor
    name
  end

end
