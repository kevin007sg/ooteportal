class Venue < ApplicationRecord
  acts_as_tenant
  
  validates_presence_of :name, :url, :street_name
  validates_uniqueness_of :name, :scope => :tenant_id
  
  has_many :events
  
  geocoded_by :street_name, latitude: :latitude, longitude: :longitude
  after_validation :geocode
  
  def venue_descriptor
      "#{name}"
  end
  
end
