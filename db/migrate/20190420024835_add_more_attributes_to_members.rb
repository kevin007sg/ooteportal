class AddMoreAttributesToMembers < ActiveRecord::Migration[5.2]
  def change
    add_column :members, :mobile_number, :string
    add_column :members, :region, :string
    add_column :members, :description, :string
    add_column :members, :street_name, :string
    add_column :members, :latitude, :float
    add_column :members, :longtitude, :float
    add_column :members, :active_status, :boolean
  end
end
