class CreateRecurrentEventEventTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :recurrent_event_event_types do |t|
      t.references :event_type, foreign_key: true
      t.references :recurrent_event, foreign_key: true

      t.timestamps
    end
  end
end
