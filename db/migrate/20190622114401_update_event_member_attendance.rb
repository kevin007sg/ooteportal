class UpdateEventMemberAttendance < ActiveRecord::Migration[5.2]
  def change
    remove_column :event_members, :attendance
    add_column :event_members, :sign_on, :time
    add_column :event_members, :sign_off, :time
  end
end
