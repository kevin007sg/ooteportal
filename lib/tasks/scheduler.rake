desc "This task is called by the Heroku scheduler add-on"
task :send_reminder => :environment do
  Event.send_reminder
  Event.send_unassigned_event_reminder
end

desc "This is a test email task"
task :test_email => :environment do
  Event.send_unassigned_event_reminder
end