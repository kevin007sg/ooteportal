require "application_system_test_case"

class SettingsTest < ApplicationSystemTestCase
  setup do
    @setting = settings(:one)
  end

  test "visiting the index" do
    visit settings_url
    assert_selector "h1", text: "Settings"
  end

  test "creating a Setting" do
    visit settings_url
    click_on "New Setting"

    check "Display phone" if @setting.display_phone
    check "Manage attendance" if @setting.manage_attendance
    check "Manage event code" if @setting.manage_event_code
    check "Manage event report" if @setting.manage_event_report
    check "Send sms" if @setting.send_sms
    fill_in "Tenant", with: @setting.tenant_id
    click_on "Create Setting"

    assert_text "Setting was successfully created"
    click_on "Back"
  end

  test "updating a Setting" do
    visit settings_url
    click_on "Edit", match: :first

    check "Display phone" if @setting.display_phone
    check "Manage attendance" if @setting.manage_attendance
    check "Manage event code" if @setting.manage_event_code
    check "Manage event report" if @setting.manage_event_report
    check "Send sms" if @setting.send_sms
    fill_in "Tenant", with: @setting.tenant_id
    click_on "Update Setting"

    assert_text "Setting was successfully updated"
    click_on "Back"
  end

  test "destroying a Setting" do
    visit settings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Setting was successfully destroyed"
  end
end
